<?php

//************
// Parse current folder to create MacroButtonConfig.xml
//**********

$filename = 'MacroButtonConfig.xml';
$dir_handle = opendir(".");


if (!$fp = fopen($filename, 'w')) {
		echo "Impossible d'ouvrir le fichier ($filename)";
		exit;
}

$content ="" ;
$content .= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
$content .= "<VBAudioVoicemeeterMacroButtonMap>\n";
$content .= "<MacroButtonConfiguration x0='22' y0='11' dx='1516' dy='809' >\n";

if (fwrite($fp, $content) === FALSE) {
	echo "Impossible d'écrire dans le fichier ($filename)";
	exit;
}

$i = 0;
$keypad = 0;

$file = readdir($dir_handle);
	
//do stuff with $file
foreach (glob("*.mp3") as $file) {
	$path_parts = pathinfo($file);
	// $path_parts['dirname'], "\n";
	// $path_parts['basename'], "\n";
	// $path_parts['extension'], "\n";
	// $path_parts['filename'], "\n";
	
	$content ="" ;
	
	$content .= "	<MacroButton index='".$i++."' type='0' color='2' key='".$keypad++."' ctrl='0' shift='0' alt='1' anyway='0' exclusive='0' trigger='0' xinput='0' >                                                \n";
	$content .= "		<MB_MIDI b1='00' b2='00' b3='00' b4='00' b5='00' b6='00' />                                                                                                                          \n";
	$content .= "		<MB_TRIGGER tchannel='0' tin='0.0' tout='0.0' tmsHold='100' tafterMute='0' />                                                                                                        \n";
	$content .= "		<MB_XINPUT nctrl='0' nbutton='0' />                                                                                                                                                  \n";
	$content .= "		<VBHIDMapItem name='Button#0' id='MBhid0' device='' vendor='0' product='0' version='0' uspa='0' us='0' nbb='0' code1='0' code2='0' code3='0' type='0' mode='0'></VBHIDMapItem>       \n";
	$content .= "		<MB_Name>".$path_parts['filename']."</MB_Name>                                                                                                                                                      \n";
	$content .= "		<MB_Subname>".$path_parts['basename']."</MB_Subname>                                                                                                                                     \n";
	$content .= "		<MB_InitRequest></MB_InitRequest>                                                                                                                                                    \n";
	$content .= "		<MB_OnRequest>Recorder.load = \"".realpath(".")."\\".$file."\"</MB_OnRequest>                                                              \n";
	$content .= "		<MB_OffRequest></MB_OffRequest>                                                                                                                                                      \n";
	$content .= "	</MacroButton>                                                                                                                                                                       \n";
	
	if (fwrite($fp, $content) === FALSE) {
		echo "Impossible d'écrire dans le fichier ($filename)";
		exit;
	}
	
}	
	
$content ="" ;
$content .= "</MacroButtonConfiguration>>\n";
$content .= "</VBAudioVoicemeeterMacroButtonMap>\n";
if (fwrite($fp, $content) === FALSE) {
	echo "Impossible d'écrire dans le fichier ($filename)";
	exit;
}

fclose($fp);
$i--;

echo "Il y a eu ".$i." macro-buttons créés dans le fichier (".realpath(".")."\\".$filename.")";
